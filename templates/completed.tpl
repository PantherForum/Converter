<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>{{ lang_convert['Convert to Panther'] }}</title>
<link rel="stylesheet" type="text/css" href="{{ stylesheet }}.css" />
</head>
<body>
<div id="pantherinstall" class="panther">
<div class="top-box"><div><!-- Top Corners --></div></div>
<div class="pantherwrap">
<div id="brdheader" class="block">
	<div class="box">
		<div id="brdtitle" class="inbox">
			<h1><span>{{ lang_convert['Panther conversion'] }}</span></h1>
			<div id="brddesc"><p>{{ lang_convert['Panther has been converted'] }}</p></div>
		</div>
	</div>
</div>
<div id="brdmain">
<div class="blockform">
	<h2><span>{{ lang_convert['Final instructions'] }}</span></h2>
	<div class="box">
		<div class="fakeform">
			<div class="inform">
				<div class="forminfo">
					<p>{{ lang_convert['Panther fully converted']|format(panther_config['o_base_url'])|raw }}</p>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
<div class="end-box"><div><!-- Bottom Corners --></div></div>
</div>
</body>
</html>