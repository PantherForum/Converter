<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" dir="ltr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>{{ lang_convert['Convert to Panther'] }}</title>
<link rel="stylesheet" type="text/css" href="{{ stylesheet }}.css" />
<body>

<div id="pantherinstall" class="panther">
<div class="top-box"><div><!-- Top Corners --></div></div>
<div class="pantherwrap">

<div id="brdheader" class="block">
	<div class="box">
		<div id="brdtitle" class="inbox">
			<h1><span>{{ lang_convert['Convert to Panther'] }}</span></h1>
			<div id="brddesc"><p>{{ lang_convert['Welcome'] }}</p></div>
		</div>
	</div>
</div>
<div class="blockform">
	<h2><span>{{ lang_convert['Convert to Panther'] }}</span></h2>
	<div class="box">
		<form id="install" method="post" action="{{ panther_config['o_base_url'] }}/convert/">
		<input type="hidden" name="form_sent" value="1" />
			<div class="inform">
{% if errors is not empty %}
				<div class="forminfo error-info">
					<h3>{{ lang_convert['Errors'] }}</h3>
					<ul class="error-list">
{% for error in errors %}
						<li><Strong>{{ error }}</strong></li>
{% endfor %}
					</ul>
				</div>
{% endif %}
			<div class="inform">
				<div class="forminfo">
					<h3>{{ lang_convert['Forum'] }}</h3>
					<p>{{ lang_convert['Info 0'] }}</p>
				</div>
				<fieldset>
					<legend>{{ lang_convert['Forum software'] }}</legend>
					<div class="infldset">
						<label class="required"><strong>{{ lang_convert['Forum'] }} <span>{{ lang_convert['Required'] }}</span></strong><br />
						<select name="forum_type">
{% for forum in forum_types %}
<option value="{{ forum['type'] }}"{% if POST['forum_type'] == forum['type'] %} selected="selected"{% endif %}>{{ forum['title'] }}</option>
{% endfor %}
						</select>
						</label>
					</div>
				</fieldset>
			</div>
			<div class="inform">
				<div class="forminfo">
					<h3>{{ lang_convert['Database setup'] }}</h3>
					<p>{{ lang_convert['Info 1'] }}</p>
				</div>
				<fieldset>
					<legend>{{ lang_convert['Database hostname'] }}</legend>
					<div class="infldset">
						<p>{{ lang_convert['Info 3']|raw }}</p>
						<label class="required"><strong>{{ lang_convert['Database server hostname'] }} <span>{{ lang_convert['Required'] }}</span></strong><br /><input type="text" name="old_db_host" value="{{ POST['old_db_host'] }}" size="50" /><br /></label>
					</div>
				</fieldset>
			</div>
			<div class="inform">
				<fieldset>
					<legend>{{ lang_convert['Database enter name'] }}</legend>
					<div class="infldset">
						<p>{{ lang_convert['Info 4'] }}</p>
						<label class="required"><strong>{{ lang_convert['Database name'] }} <span>{{ lang_convert['Required'] }}</span></strong><br /><input id="req_db_name" type="text" name="old_db_name" value="{{ POST['old_db_name'] }}" size="30" /><br /></label>
					</div>
				</fieldset>
			</div>
			<div class="inform">
				<fieldset>
					<legend>{{ lang_convert['Database enter information'] }}</legend>
					<div class="infldset">
						<p>{{ lang_convert['Info 5'] }}</p>
						<label class="conl">{{ lang_convert['Database username'] }}<br /><input type="text" name="old_db_username" value="{{ POST['old_db_username'] }}" size="30" /><br /></label>
						<label class="conl">{{ lang_convert['Database password 1'] }}<br /><input type="password" name="old_db_password1" size="30" /><br /></label>
						<label class="conl">{{ lang_convert['Database password 2'] }}<br /><input type="password" name="old_db_password2" size="30" /><br /></label>
						<div class="clearer"></div>
					</div>
				</fieldset>
			</div>
			<div class="inform">
				<fieldset>
					<legend>{{ lang_convert['Database enter prefix'] }}</legend>
					<div class="infldset">
						<p>{{ lang_convert['Info 6'] }}</p>
						<label>{{ lang_convert['Table prefix'] }}<br /><input id="db_prefix" type="text" name="old_db_prefix" value="{{ POST['old_db_prefix'] }}" size="20" maxlength="30" /><br /></label>
					</div>
				</fieldset>
			</div>
			<div class="inform">
				<div class="forminfo">
					<h3>{{ lang_convert['Avatar conversion'] }}</h3>
					<p>{{ lang_convert['Info 7'] }}</p>
				</div>
				<fieldset>
					<legend>{{ lang_convert['Avatar path'] }}</legend>
					<div class="infldset">
						<p>{{ lang_convert['Info 8']|raw }}</p>
						<label class="required"><strong>{{ lang_convert['Avatar path'] }} </strong><br /><input type="text" name="avatar_path" value="{{ POST['avatar_path'] }}" size="50" /><br /></label>
					</div>
				</fieldset>
			</div>
			<div class="inform">
				<div class="forminfo">
					<h3>{{ lang_convert['Additional options'] }}</h3>
					<p>{{ lang_convert['Info 9'] }}</p>
				</div>
				<fieldset>
					<legend>{{ lang_convert['Database limit'] }}</legend>
					<div class="infldset">
						<p>{{ lang_convert['Info 10'] }}</p>
						<label class="required"><strong>{{ lang_convert['Limit'] }} </strong><br /><input type="text" name="limit" value="{% if POST['limit'] is not none %}{{ POST['limit'] }}{% else %}500{% endif %}" size="5" /><br /></label>
						<p>{{ lang_convert['Info 11'] }}</p>
						<label class="required"><strong>{{ lang_convert['Attach path'] }} </strong><br /><input type="text" name="attach_path" value="{{ POST['attach_path'] }}" size="50" /><br /></label>
					</div>
				</fieldset>
			</div>
			<p class="warning">{{ lang_convert['Convert warning']|raw }}</p>
			<p class="buttons"><input type="submit" name="start" value="{{ lang_convert['Start conversion'] }}" /></p>
		</form>
	</div>
</div>
</div>

</div>
<div class="end-box"><div><!-- Bottom Corners --></div></div>
</div>

</body>
</html>