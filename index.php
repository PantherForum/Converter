<?php
define('PANTHER_ROOT', __DIR__.'/../');
if (!file_exists(PANTHER_ROOT.'include/common.php'))
	exit('In order to convert, you must first have an existing installation of Panther, and place the conversion files in a sub-folder named \'convert\'.');

session_start();
require PANTHER_ROOT.'include/common.php';
require PANTHER_ROOT.'convert/classes/panther.php';

header('Expires: Thu, 21 Jul 1977 07:30:00 GMT');
header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
header('Cache-Control: post-check=0, pre-check=0', false);
header('Pragma: no-cache');
header('Content-type: text/html; charset=utf-8');
header('X-Frame-Options: deny');

$loader->addPath(PANTHER_ROOT.'convert/templates/', 'convert');
if (file_exists(PANTHER_ROOT.'convert/lang/'.$panther_user['language'].'.php'))
	require PANTHER_ROOT.'convert/lang/'.$panther_user['language'].'.php';
else
	require PANTHER_ROOT.'convert/lang/English.php';

function forum_types()
{
	return array(
		array('type' => 'fluxbb', 'title' => 'FluxBB (1.2/1.3/1.4/1.5)'),
		array('type' => 'luna', 'title' => 'Luna (1.0/1.1/1.2/1.3)'),
		array('type' => 'punbb', 'title' => 'PunBB (1.2/1.3/1.4)'),
		array('type' => 'mybb', 'title' => 'MyBB (1.8)'),
		array('type' => 'smf', 'title' => 'Simple Machines Forum (2.0)'),
	);
}

$errors = array();
if (isset($_POST['form_sent']) || isset($_GET['do']) && isset($_SESSION[$panther_config['o_cookie_name']]) && isset($_GET['csrf_token']))
{
	if (isset($_POST['form_sent']))
	{
		$old_db_host = isset($_POST['old_db_host']) ? panther_trim($_POST['old_db_host']) : '';
		$old_db_name = isset($_POST['old_db_name']) ? panther_trim($_POST['old_db_name']) : '';
		$old_db_username = isset($_POST['old_db_username']) ? panther_trim($_POST['old_db_username']) : '';
		$old_db_password = isset($_POST['old_db_password1']) ? panther_trim($_POST['old_db_password1']) : '';
		$old_db_password2 = isset($_POST['old_db_password2']) ? panther_trim($_POST['old_db_password2']) : '';
		$old_db_prefix = isset($_POST['old_db_prefix']) ? panther_trim($_POST['old_db_prefix']) : '';
		$forum_type = isset($_POST['forum_type']) ? panther_trim($_POST['forum_type']) : '';
		$avatar_path = isset($_POST['avatar_path']) ? panther_trim($_POST['avatar_path']) : '';
		$attach_path = isset($_POST['attach_path']) ? panther_trim($_POST['attach_path']) : '';
		$limit = isset($_POST['limit']) ? intval($_POST['limit']) : 500;
		$do = '';
		$start = 0;
		
		if ($avatar_path != '' && substr($avatar_path, -1) != '/')
			$avatar_path .= '/';
	}
	else
	{
		// Improvise for a CSRF token - cookie names should be unique per board, and we don't have any way of making sure it IS an admin
		if (sha1($panther_config['o_cookie_name']) != $_GET['csrf_token'])
			message($lang_common['Bad referrer']);

		$do = panther_trim($_GET['do']);
		$start = isset($_GET['start']) ? intval($_GET['start']) : 0;
		$limit = isset($_GET['limit']) ? intval($_GET['limit']) : 500;
		foreach ($_SESSION as $variable => $data)
			$$variable = $data;
	}

	if ($avatar_path != '')
	{
		define('CONVERT_AVATARS', true);
		if (substr($avatar_path, -1) != '/')
			$avatar_path .= '/';
	}
	
	if ($attach_path != '')
	{
		define('CONVERT_ATTACHMENTS', true);
		if (substr($attach_path, -1) != '/')
			$attach_path .= '/';
	}

	if ($old_db_password != $old_db_password2)
		$errors[] = $lang_convert['Passwords do not match'];

	if ($old_db_host == '')
		$errors[] = $lang_convert['DB host not valid'];

	if ($old_db_name == '')
		$errors[] = $lang_convert['DB name not valid'];	

	if ($old_db_username == '')
		$errors[] = $lang_convert['DB username not valid'];

	if ($limit < 2)
		$errors[] = $lang_convert['Limit too short'];

	if (empty($errors))
	{
		$config = array(
			'host' => $old_db_host,
			'prefix' => $old_db_prefix,
			'username' => $old_db_username,
			'password' => $old_db_password,
			'db_name' => $old_db_name,
			'p_connect' => false,
		);

		$old_db = new db($config);
		if (isset($_POST['form_sent']))
		{
			$db_info = array(
				$panther_config['o_cookie_name'] => true,
				'old_db_host' => $old_db_host,
				'old_db_name' => $old_db_name,
				'old_db_username' => $old_db_username,
				'old_db_password' => $old_db_password,
				'old_db_password2' => $old_db_password2,
				'old_db_prefix' => $old_db_prefix,
				'forum_type' => $forum_type,
				'avatar_path' => $avatar_path,
				'attach_path' => $attach_path,
				'limit' => $limit,
			);

			foreach ($db_info as $variable => $data)
				$_SESSION[$variable] = $data;
		}

		$panther = new panther($db, $old_db, $panther_config, $lang_convert, $panther_user, $forum_type, $avatar_path, $attach_path, $tpl_manager);
		$redirect = $panther->convert($do, $start, $limit);

		if ($redirect == false)
			message('Invalid response returned from conversion step '.$do.'.');
		else
		{
			if ($redirect[0] == 'complete')
			{
				$db->end_transaction();
				$panther->complete();
			}
			else
			{
				$redirect[1] = isset($redirect[1]) ? $redirect[1] : 0;
				redirect($panther_config['o_base_url'].'/convert/?do='.$redirect[0].'&start='.$redirect[1].'&limit='.$limit.'&csrf_token='.sha1($panther_config['o_cookie_name']), $lang_convert['Redirecting']);
			}
		}
	}
}

// First run - means that the database will be emptied; best check who this is
if (!$panther_user['is_admin'])
	message($lang_convert['No permission']);

$types = forum_types();
asort($types);
$tpl = $tpl_manager->loadTemplate('@convert/index.tpl');
echo $tpl->render(
	array(
		'lang_convert' => $lang_convert,
		'errors' => $errors,
		'panther_config' => $panther_config,
		'forum_types' => $types,
		'POST' => $_POST,
		'stylesheet' => (($panther_config['o_style_dir']) != '' ? $panther_config['o_style_dir'] : $panther_config['o_base_url'].'/style/').$panther_user['style'],
	)
);