<?php
if (!defined('PANTHER'))
	exit;

class panther
{
	public $avatar_mimes = array(
		'image/jpg' => 'jpg',
		'image/gif' => 'gif',
		'image/png' => 'png'
	);

	public $avatar_exts = array(
		'jpg',
		'gif',
		'png',
	);

	public function __construct($db, $old_db, $panther_config, $lang_convert, $panther_user, $forum, $avatar_path, $attach_path, $tpl_manager)
	{
		$this->db = $db;
		$this->old_db = $old_db;
		$this->panther_config = $panther_config;
		$this->attach_path = $attach_path;
		$this->lang = $lang_convert;
		$this->panther_user = $panther_user;
		$this->tpl_manager = $tpl_manager;
		$this->avatars_dir = ($panther_config['o_avatars_dir'] != '') ? $panther_config['o_avatars_path'] : PANTHER_ROOT.$panther_config['o_avatars_path'].'/';
		$this->avatar_path = $avatar_path;

		if (!class_exists($forum))
		{
			require PANTHER_ROOT.'convert/classes/software/'.$forum.'.php';
			$this->forum = new $forum($db, $old_db, $panther_config, $lang_convert, $panther_user, $forum, $avatar_path, $attach_path, $tpl_manager);
		}
	}

	/**
	 * Clears the avatar directory
	 */
	 private function clear_avatars()
	 {
		$num_deleted = 0;
		$avatars =  array_diff(scandir($this->avatars_dir), array('.', '..', '1.'.$this->panther_config['o_avatar']));
		foreach ($avatars as $avatar)
		{
			@unlink($this->avatars_dir.$avatar);
			++$num_deleted;
		}

		if ($num_deleted)
			$this->log('Deleted '.$num_deleted.' avatars');
	}

	/**
	 * Clears the attachment directory
	 */
	 private function clear_attachments()
	 {
		$num_deleted = 0;
		$attachments =  array_diff(scandir($this->panther_config['o_attachments_dir']), array('.', '..'));
		foreach ($attachments as $cur_attach)
		{
			@unlink($this->panther_config['o_attachments_dir'].$cur_attach);
			++$num_deleted;
		}

		if ($num_deleted)
			$this->log('Deleted '.$num_deleted.' attachments');
	}

	/**
	 * Clear all Panther database tables
	 */
	private function clear_tables()
	{
		$tables = array(
			'announcements',
			'attachments',
			'bans',
			'blocks',
			'categories',
			'censoring',
			'conversations',
			'extensions',
			'extension_code',
			'forums',
			'forum_perms',
			'forum_subscriptions',
			'login_queue',
			'messages',
			'multi_moderation',
			'online',
			'pms_data',
			'polls',
			'posts',
			'ranks',
			'reports',
			'reputation',
			'restrictions',
			'robots',
			'search_cache',
			'search_matches',
			'search_words',
			'smilies',
			'topics',
			'topic_subscriptions',
			'warnings',
			'warning_levels',
			'warning_types',
		);

		foreach ($tables as $cur_table)
			$this->db->delete($cur_table, '1=1');

		$this->db->delete('users', 'id>1');
	}

	/**
	 * Check for more work to be done
	 */
	public function redirect($table, $field, $start)
	{
		$data = array(
			':start' => $start,
		);

		$ps = $this->old_db->select($table, 1, $data, $field.'>:start LIMIT 1');
		if ($ps->rowCount())
			return $start;

		return false;
	}

	/**
	 * Run the current conversion step
	 */
	public function convert($action = '', $start = 0, $limit = 500)
	{
		if (!method_exists($this->forum, $action) && $action != '')
			exit('Invalid conversion action.');

		$steps = array_values($this->forum->steps);

		if ($action == '')
		{
			$action = $steps[0];
			$this->clear_tables();
			$this->clear_avatars();
			$this->clear_attachments();
		}

		if (is_callable(array($this->forum, $action)))
			$redirect_to = call_user_func_array(array($this->forum, $action), array($start, $limit));
		else
			$redirect_to = null;

		if ($redirect_to != null)
			return array($action, $redirect_to);

		$current_step = array_search($action, $steps);

		// This should never happen if it's using valid steps
		if ($current_step === false)
			return false;

		if (!isset($steps[++$current_step]))
			return array('complete');

		return array($steps[$current_step]);
	}

	/**
	 * Save the avatar of a user into the Panther upload directory
	 */
	protected function save_avatar($file, $user_id)
	{
		if (!defined('CONVERT_AVATARS'))
			return;

		// Download remote file
		if (strpos($file, '://') !== false)
		{
			$extension = strtolower(substr($file, strrpos($file, '.') +1));

			// Download avatar to temporary location (cache directory)
			$tmp_file = FORUM_CACHE_DIR.uniqid().uniqid();
			if (!file_put_contents($tmp_file, file_get_contents($file)))
			{
				$this->log('Unable to get avatar for user ID '.$user_id.' using remote file transfer');
				return false;
			}

			// Check image mime type
			$info = @getimagesize($tmp_file);
			if (!isset($info['mime']))
			{
				$this->log('Unable to get image mime type for file '.$tmp_file);
				unlink($tmp_file);
				return false;
			}
			else if (!isset($this->avatar_mimes[$info['mime']]))
			{
				$this->log('Invalid avatar type for avatar '.$tmp_file);
				unlink($tmp_file);
				return false;
			}

			$extension = $this->avatar_mimes[$info['mime']];
			if (!rename($tmp_file, $this->avatars_dir.$user_id.'.'.$extension))
			{
				$this->log('Unable to rename file '.$tmp_file.' to '.$this->avatars_dir.$user_id.'.'.$extension);
				return false;
			}
		}
		else if (file_exists($file))
		{
			$actual_file = str_replace($this->avatar_path, '', $file);
			$extension = strtolower(substr($actual_file, strrpos($actual_file, '.')+1));
			if (!in_array($extension, $this->avatar_exts))
			{
				$this->log('Invalid avatar type for user ID '.$user_id);
				return false;
			}

			if (!copy($file, $this->avatars_dir.$user_id.'.'.$extension))
			{
				$this->log('Unable to copy file '.$file.' to '.$this->avatars_dir.$user_id.'.'.$extension);
				return false;
			}
		}

		return true;
	}

	/**
	* Saves an attachment to the Panther attachment directory
	*/
	protected function save_attachment($file, $renamed)
	{
		if (!defined('CONVERT_ATTACHMENTS'))
			return;

		if (!copy($this->attach_path.$file, $this->panther_config['o_attachments_dir'].$renamed))
		{
			$this->log('Unable to copy attachment '.$this->attach_path.$file.' to '.$this->panther_config['o_attachments_dir'].$renamed);
			return false;
		}
		
		return true;
	}

	/**
	* Log a message to the conversion file
	*/
	public function log($error)
	{
		if (!isset($this->fp))
			$this->fp = fopen("convert.log", "w");

		fwrite($this->fp, '['.date('j-M-Y H:i:s').'] '.$error.PHP_EOL);
	}

	/**
	 * Install the latest version of the password conversion extension for the forum software they are converting from
	 */
	private function install_extension()
	{
		if (!file_put_contents(PANTHER_ROOT.PANTHER_ADMIN_DIR.'/extensions/convert-passwords.xml', file_get_contents('http://extensions.pantherforum.org/'.$this->forum->file.'.xml')))
		{
			$this->log('Unable to download extension for password conversion. Download manually and submit ticket for further assistance.');
			return;
		}

		$content = file_get_contents(PANTHER_ROOT.PANTHER_ADMIN_DIR.'/extensions/convert-passwords.xml');
		$extension = xml_to_array($content);
		$extension = $extension['extension'];

		$insert = array(
			'id' => 'convert-passwords',
			'title' => $extension['title'],
			'version' => $extension['version'],
			'description' => $extension['description'],
			'author' => $extension['author'],
			'uninstall_note' => (isset($extension['uninstall_note']) ? $extension['uninstall_note'] : ''),
			'uninstall' => (isset($extension['uninstall']) ? $extension['uninstall'] : ''),
			'enabled' => 1,
		);

		$this->db->insert('extensions', $insert);
		$extension_id = $this->db->lastInsertId($this->db->prefix.'extensions');

		foreach ($extension['hooks']['hook'] as $hook)
		{
			$insert = array(
				'extension_id' => 'convert-passwords',
				'hook' => $hook['attributes']['id'],
				'code' => panther_trim($hook['content']),
			);

			$this->db->insert('extension_code', $insert);
		}
	}

	/**
	 * Complete the converter
	 */
	public function complete()
	{
		// Install the latest extension for password conversion
		$this->install_extension();

		forum_clear_cache();
		$tpl = $this->tpl_manager->loadTemplate('@convert/completed.tpl');
		echo $tpl->render(
			array(
				'lang_convert' => $this->lang,
				'stylesheet' => (($this->panther_config['o_style_dir']) != '' ? $this->panther_config['o_style_dir'] : $this->panther_config['o_base_url'].'/style/').$this->panther_user['style'],
				'panther_config' => $this->panther_config,
			)
		);

		exit;
	}
}