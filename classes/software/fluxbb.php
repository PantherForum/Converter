<?php
if (!defined('PANTHER'))
	exit;

class fluxbb extends panther
{
	public $steps = array(
		'bans',
		'categories',
		'censoring',
		'forums',
		'forum_perms',
		'forum_subscriptions',
		'posts',
		'reports',
		'ranks',
		'topics',
		'topic_subscriptions',
		'config',
		'groups',
		'users',
	);

	public $file = 'fluxbb-password-converter';
	private function batch_move($table)
	{
		$ps = $this->old_db->select($table);
		foreach ($ps as $cur_row)
			$this->db->insert($table, $cur_row);
	}

	private function correct_group($group_id)
	{
		switch ($group_id)
		{
			case 0:
			case 1:

			break;
			case 2:
				$group_id = 3;
			break;
			case 3:
				$group_id = 4;
			break;
			case 4:
				$group_id = 5;
			break;
			default: // Increment every other group because we have a 'new members' group as 6
				$group_id++;
			break;
		}

		return $group_id;
	}

	public function bans()
	{
		$this->batch_move('bans');
	}

	public function categories()
	{
		$this->batch_move('categories');
	}

	public function censoring()
	{
		$this->batch_move('censoring');
	}

	public function config()
	{
		$ps = $this->old_db->select('config');
		foreach ($ps as $value)
		{
			switch ($value['conf_name'])
			{
				case 'o_board_title':
				case 'o_board_desc':
				case 'o_default_timezone':
				case 'o_time_format':
				case 'o_date_format':
				case 'o_timeout_visit':
				case 'o_timeout_online':
				case 'o_redirect_delay':
				case 'o_show_version':
				case 'o_show_user_info':
				case 'o_show_post_count':
				case 'o_signatures':
				case 'o_smilies':
				case 'o_smilies_sig':
				case 'o_make_links':
				case 'o_topic_review':
				case 'o_disp_topics_default':
				case 'o_disp_posts_default':
				case 'o_indent_num_spaces':
				case 'o_quote_depth':
				case 'o_quickpost':
				case 'o_users_online':
				case 'o_censoring':
				case 'o_show_dot':
				case 'o_topic_views':
				case 'o_quickjump':
				case 'o_gzip':
				case 'o_additional_navlinks':
				case 'o_report_method':
				case 'o_regs_report':
				case 'o_default_email_setting':
				case 'o_mailing_list':
				case 'o_avatars':
				case 'o_avatars_width':
				case 'o_avatars_height':
				case 'o_avatars_size':
				case 'o_search_all_forums':
				case 'o_admin_email':
				case 'o_webmaster_email':
				case 'o_forum_subscriptions':
				case 'o_topic_subscriptions':
				case 'o_smtp_host':
				case 'o_smtp_user':
				case 'o_smtp_pass':
				case 'o_smtp_ssl':
				case 'o_regs_allow':
				case 'o_regs_verify':
				case 'o_announcement':
				case 'o_announcement_message':
				case 'o_rules':
				case 'o_rules_message':
				case 'o_default_dst':
				case 'o_feed_type':
				case 'o_feed_ttl':
				case 'p_message_bbcode':
				case 'p_message_img_tag':
				case 'p_message_all_caps':
				case 'p_subject_all_caps':
				case 'p_sig_all_caps':
				case 'p_sig_bbcode':
				case 'p_sig_img_tag':
				case 'p_sig_length':
				case 'p_sig_lines':
				case 'p_allow_banned_email':
				case 'p_allow_dupe_email':
				case 'p_force_guest_email':
					// Do nothing, these are valid keys
				break;
				default:
					continue 2;
				break;
			}

			$update = array(
				'conf_value' => $value['conf_value'],
			);

			$data = array(
				':conf_name' => $value['conf_name'],
			);

			$this->db->update('config', $update, 'conf_name=:conf_name', $data);
		}
	}

	public function forums()
	{
		$this->batch_move('forums');
	}

	public function forum_perms()
	{
		$ps = $this->old_db->select('forum_perms');
		foreach ($ps as $cur_row)
		{
			$cur_row['group_id'] = $this->correct_group($cur_row['group_id']);
			$this->db->insert('forum_perms', $cur_row);
		}
	}

	public function forum_subscriptions()
	{
		// The <= 1.4 branch does not have this feature
		if ($this->old_db->table_exists('forum_subscriptions'))
			$this->batch_move('forum_subscriptions');
	}

	public function topics($start, $limit)
	{
		$data = array(
			':start' => $start,
			':limit' => $limit,
		);

		$ps = $this->old_db->select('topics', '*', $data, 'id>:start ORDER BY id ASC LIMIT :limit');
		foreach ($ps as $cur_topic)
		{
			$start = $cur_topic['id'];
			$this->db->insert('topics', $cur_topic);
		}

		return $this->redirect('topics', 'id', $start);
	}

	public function posts($start, $limit)
	{
		$data = array(
			':start' => $start,
			':limit' => $limit,
		);

		$ps = $this->old_db->select('posts', '*', $data, 'id>:start ORDER BY id ASC LIMIT :limit');
		foreach ($ps as $cur_post)
		{
			$start = $cur_post['id'];
			$this->db->insert('posts', $cur_post);
		}

		return $this->redirect('posts', 'id', $start);
	}

	public function ranks()
	{
		// The 1.5 branch does not have this feature
		if ($this->old_db->table_exists('ranks'))
			$this->batch_move('ranks');
	}

	public function reports()
	{
		$this->batch_move('reports');
	}

	public function topic_subscriptions()
	{
		if ($this->old_db->table_exists('subscriptions')) // Then we're using either the 1.3 legacy or 1.2 branch
		{
			$ps = $this->old_db->select('subscriptions');
			foreach ($ps as $cur_subscription)
				$this->db->insert('topic_subscriptions', $cur_subscription);
		}
		else
			$this->batch_move('topic_subscriptions');
	}
	
	public function groups()
	{
		$ps = $this->old_db->select('groups');
		foreach ($ps as $cur_group)
		{
			if (isset($cur_group['g_edit_subjects_interval'])) // 1.2 used this
			{
				$cur_group['g_edit_subject'] = $cur_group['g_edit_subjects_interval'];
				unset($cur_group['g_edit_subjects_interval']);
			}

			$cur_group['g_id'] = $this->correct_group($cur_group['g_id']);
			$data = array(
				':id' => $cur_group['g_id'],
			);

			$ps1 = $this->db->select('groups', 1, $data, 'g_id=:id');
			if ($ps1->rowCount())
				$this->db->update('groups', $cur_group, 'g_id=:id', $data);
			else
				$this->db->insert('groups', $cur_group);
		}
	}

	public function users($start, $limit)
	{
		$data = array(
			':start' => $start,
			':limit' => $limit,
		);

		$ps = $this->old_db->select('users', '*', $data, 'id!=1 AND id>:start ORDER BY id ASC LIMIT :limit');
		foreach ($ps as $cur_user)
		{
			if (isset($cur_user['use_avatar'])) // 1.2 branch used the following three columns
			{
				unset($cur_user['use_avatar']);
				unset($cur_user['save_pass']);
				unset($cur_user['save_pass']);
			}

			$start = $cur_user['id'];
			$cur_user['group_id'] = $this->correct_group($cur_user['group_id']);
			unset($cur_user['jabber']);
			unset($cur_user['icq']);
			unset($cur_user['aim']);
			unset($cur_user['yahoo']);
			unset($cur_user['msn']);

			$this->db->insert('users', $cur_user);
			generate_login_key($cur_user['id']);

			$filetypes = array('jpg', 'gif', 'png');
			foreach ($filetypes as $cur_type)
			{
				$path = $this->avatar_path.$cur_user['id'].'.'.$cur_type;
				if (file_exists($path) && $img_size = getimagesize($path))
					$this->save_avatar($path, $cur_user['id']);
			}
		}

		$this->redirect('users', 'id', $start);
	}
}