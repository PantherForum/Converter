<?php
if (!defined('PANTHER'))
	exit;

class smf extends panther
{
	public $steps = array(
		'attachments',
		'bans',
		'forums',
		'forum_perms',
		'categories',
		'subscriptions',
		'reports',
		'topics',
		'posts',
		'smileys',
		'config',
		'groups',
		'users',
	);

	public $file = 'smf-password-converter';
	private function correct_group($group_id)
	{
		switch($group_id)
		{
			case 0:
			case 1:
			case 2:
			case 3:
				
			break;
			default:
				++$group_id;
			break;
		}

		return $group_id;
	}

	public function attachments()
	{
		$ps = $this->old_db->select('attachments', 'id_attach AS id, id_msg AS post_id, id_folder AS dir, id_member+1 AS owner, filename, fileext AS extension, file_hash AS location, size, downloads');
		foreach ($ps as $cur_attach)
		{
			$cur_attach['mime'] = attach_create_mime($cur_attach['extension']);
			$this->save_attachment($cur_attach['id'].'_'.$cur_attach['location'], $cur_attach['location'].'.attach');
			$cur_attach['location'] .= '.attach';

			unset($cur_attach['dir']);
			$this->db->insert('attachments', $cur_attach);
		}
	}

	public function bans()
	{
		$ps = $this->old_db->run('SELECT b.name AS username, b.expire_time AS expire, b.reason AS message, i.email_address AS email FROM '.$this->old_db->prefix.'ban_groups AS b INNER JOIN '.$this->old_db->prefix.'ban_items AS i ON b.id_ban_group=i.id_ban_group');
		foreach ($ps as $cur_ban)
			$this->db->insert('bans', $cur_ban);
	}

	public function forums()
	{
		$ps = $this->old_db->select('boards', 'id_board AS id, id_cat AS cat_id, id_parent AS parent_forum, board_order AS disp_position, id_last_msg AS last_post_id, id_profile, name AS forum_name, description AS forum_desc, num_topics, num_posts, redirect AS redirect_url');
		foreach ($ps as $cur_forum)
		{
			$data = array(
				':id' => $cur_forum['id_profile'],
			);

			$ps1 = $this->old_db->select('members', 'member_name AS last_poster', $data, 'id_member=:id');
			$cur_forum['last_poster'] = $ps1->fetchColumn();
			unset($cur_forum['id_profile']);

			// Make sure it's properly set to NULL
			if ($cur_forum['redirect_url'] == '')
				$cur_forum['redirect_url'] = null;
			else if ($cur_forum['redirect_url'] != '')
				$cur_forum['last_poster'] = null;

			$data = array(
				':id' => $cur_forum['id'],
			);

			$cur_forum['moderators'] = array();
			$ps1 = $this->old_db->run('SELECT m.id_member AS user_id, u.id_group AS group_id, u.member_name AS username FROM '.$this->old_db->prefix.'moderators AS m INNER JOIN '.$this->old_db->prefix.'members AS u ON m.id_member=u.id_member WHERE id_board=:id', $data);
			foreach ($ps1 as $cur_moderator)
			{
				$cur_forum['moderators'][$cur_moderator['username']] = $cur_moderator['user_id'];
				if (!isset($cur_forum['moderators']['groups']))
					$cur_forum['moderators']['groups'] = array();

				$cur_moderator['group_id'] = $this->correct_group($cur_moderator['group_id']);
				$cur_forum['moderators']['groups'][$cur_moderator['user_id']] = $cur_moderator['group_id'];
			}

			$data = array(
				':id' => $cur_forum['last_post_id'],
			);

			$ps1 = $this->old_db->select('messages', 'id_topic, poster_time', $data, 'id_msg=:id');
			list($cur_forum['last_topic_id'], $cur_forum['last_post']) = $ps1->fetch(PDO::FETCH_NUM);

			$cur_forum['moderators'] = (!empty($cur_forum['moderators'])) ? serialize($cur_forum['moderators']) : null;
			$this->db->insert('forums', $cur_forum);
		}
	}

	public function groups()
	{
		$ps = $this->old_db->select('membergroups', 'id_group AS g_id, group_name AS g_title, online_color AS g_colour, min_posts AS g_promote_min_posts, id_group+1 AS g_promote_next_group');
		foreach ($ps as $cur_group)
		{
			$data = array(
				':id' => $cur_group['g_promote_next_group'],
			);

			$ps1 = $this->old_db->select('membergroups', 1, $data, 'id_group=:id');
			if (!$ps1->rowCount())
				$cur_group['g_promote_next_group'] = 0;

			$data = array(
				':id' => $cur_group['g_id'],
			);

			$ps1 = $this->db->select('groups', 1, $data, 'g_id=:id');
			if ($ps1->rowCount())
				$this->db->update('groups', $cur_group, 'g_id=:id', $data);
			else
				$this->db->insert('groups', $cur_group);
		}
	}

	public function forum_perms()
	{
		$ps = $this->old_db->select('board_permissions', 'permission,  id_group AS group_id, add_deny', array(), 'id_group!=0');
		foreach ($ps as $cur_group)
		{
			switch($cur_group['permission'])
			{
				case 'delete_own':
					$key = 'g_delete_posts';
				break;
				case 'poll_post':
					$key = 'g_post_polls';
				break;
				case 'post_attachment':
					$key = 'g_attach_files';
				break;
				case 'post_reply_any':
					$key = 'g_post_replies';
				break;
				default:
					continue 2;
				break;
			}

			$cur_group['group_id'] = $this->correct_group($cur_group['group_id']);
			$update = array(
				$key => $cur_group['add_deny'],
			);

			$data = array(
				':id' => $cur_group['group_id'],
			);

			$this->db->update('groups', $update, 'g_id=:id', $data);
		}
	}

	public function categories()
	{
		$ps = $this->old_db->select('categories', 'id_cat AS id, cat_order AS disp_position, name AS cat_name');
		foreach ($ps as $cur_category)
			$this->db->insert('categories', $cur_category);
	}

	public function subscriptions()
	{
		$ps = $this->old_db->select('log_notify', 'id_member+1 AS user_id, id_topic AS topic_id, id_board AS forum_id');
		foreach ($ps as $cur_subscription)
		{
			if (!$cur_subscription['forum_id'])
			{
				unset($cur_subscription['forum_id']);
				$this->db->insert('topic_subscriptions', $cur_subscription);
			}
			else
			{
				unset($cur_subscription['topic_id']);
				$this->db->insert('forum_subscriptions', $cur_subscription);
			}
		}
	}

	public function reports()
	{
		$ps = $this->old_db->select('log_reported', 'id_report AS id, id_msg AS post_id, id_topic AS topic_id, id_board AS forum_id, id_member+1 AS reported_by, time_started AS created');
		foreach ($ps as $cur_report)
			$this->db->insert('reports', $cur_report);
	}

	public function users()
	{
		$ps = $this->old_db->select('members', 'id_member+1 AS id, member_name AS username, date_registered AS registered, posts AS num_posts, id_group AS group_id, real_name AS realname, last_login AS last_visit, passwd AS password, email_address AS email, website_url AS url, location, time_format, signature, usertitle AS title, member_ip AS registration_ip, password_salt AS salt');
		foreach ($ps as $cur_user)
		{
			$cur_user['group_id'] = $this->correct_group($cur_user['group_id']);
	
			$this->db->insert('users', $cur_user);
			generate_login_key($cur_user['id']);
		}
	}

	public function topics()
	{
		$ps = $this->old_db->run('SELECT t.id_topic AS id, t.is_sticky AS sticky, t.id_board AS forum_id, t.id_first_msg AS first_post_id, t.id_last_msg AS last_post_id, t.id_member_updated+1 AS last_poster_id, t.num_replies, t.num_views, t.locked AS closed, t.approved, m.poster_name AS poster, m.subject, m.poster_time AS posted, m2.poster_time AS last_post FROM '.$this->old_db->prefix.'topics AS t INNER JOIN '.$this->old_db->prefix.'messages AS m ON t.id_first_msg=m.id_msg INNER JOIN '.$this->old_db->prefix.'messages AS m2 ON t.id_last_msg=m2.id_msg');
		foreach ($ps as $cur_topic)
		{
			$data = array(
				':id' => $cur_topic['last_poster_id'],
			);

			$ps1 = $this->old_db->select('members', 'member_name', $data, 'id_member=:id');
			$cur_topic['last_poster'] = $ps1->fetchColumn();

			unset($cur_topic['last_poster_id']);
			$this->db->insert('topics', $cur_topic);
		}
	}

	public function posts()
	{
		$search = array(
			'[li]' => '[*]',
			'[/li]' => '[/*]',
			'[tr]' => '[list]',
			'[/tr]' => '[/list]',
			'[td]' => '[*]',
			'[/td]' => '[/*]',
			'<br />' => "\n",
			'[hr]' => "\n",
			'::)' => ':rolleyes:',
			'&nbsp;' => ' ',
		);

		$ps = $this->old_db->run('SELECT id_msg AS id, id_topic AS topic_id, poster_time AS posted, id_member+1 AS poster_id, poster_name AS poster, poster_email, poster_ip, smileys_enabled AS hide_smilies, modified_time AS edited, modified_name AS edited_by, body AS message, approved FROM '.$this->old_db->prefix.'messages');
		foreach ($ps as $cur_post)
		{
			if ($cur_post['poster_id'] > 1)
				$cur_post['poster_email'] = null;

			$cur_post['message'] = strip_tags(str_replace(array_keys($search), array_values($search), $cur_post['message']));
			$this->db->insert('posts', $cur_post);
		}
	}

	public function smileys()
	{
		$ps = $this->old_db->select('smileys', 'id_smiley AS id, code, filename AS image, smiley_order AS disp_position');
		foreach ($ps as $cur_smiley)
			$this->db->insert('smilies', $cur_smiley);
	}

	public function config()
	{
		$ps = $this->old_db->select('settings', 'variable AS conf_name, value AS conf_value');
		foreach ($ps as $cur_config)
		{
			switch ($cur_config['conf_name'])
			{
				case 'enableCompressedOutput':
					$key = 'o_gzip';
				break;
				case 'attachmentExtensions':
					$key = 'o_attachment_images';
				break;
				case 'onlineEnable':
					$key = 'o_users_online';
				break;
				case 'smtp_username':
					$key = 'o_smtp_user';
				break;
				case 'smtp_password':
					$key = 'o_smtp_pass';
				break;
				case 'smileys_dir':
					$key = 'o_smiley_path';
				break;
				case 'smileys_url':
					$key = 'o_smiley_dir';
				break;
				case 'avatar_max_height_upload':
					$key = 'o_avatars_height';
				break;
				case 'avatar_max_width_upload':
					$key = 'o_avatars_width';
				break;
				case 'enableBBC':
					$key = 'p_message_bbcode';
				break;
				case 'reg_verification':
					$key = 'o_regs_verify';
				break;
				default:
					continue 2;
				break;
			}

			$update = array(
				'conf_value' => $cur_config['conf_value'],
			);

			$data = array(
				':key' => $key,
			);

			$this->db->update('config', $update, 'conf_name=:key', $data);
		}
	}
}