<?php
if (!defined('PANTHER'))
	exit;

class punbb extends panther
{
	public $steps = array(
		'bans',
		'categories',
		'censoring',
		'forums',
		'forum_perms',
		'forum_subscriptions',
		'posts',
		'reports',
		'ranks',
		'topics',
		'config',
		'groups',
		'users',
	);

	public $file = 'punbb-password-converter';
	private function batch_move($table)
	{
		$ps = $this->old_db->select($table);
		foreach ($ps as $cur_row)
			$this->db->insert($table, $cur_row);
	}

	private function correct_group($group_id)
	{
		switch ($group_id)
		{
			case 0:
			case 1:

			break;
			case 2:
				$group_id = 4;
			break;
			case 3:
				$group_id = 5;
			break;
			case 4:
				$group_id = 3;
			break;
			default: // Increment every other group - we have two additional groups with the IDs of 5 & 6
				$group_id++;
				$group_id++;
			break;
		}

		return $group_id;
	}

	public function bans()
	{
		$this->batch_move('bans');
	}

	public function categories()
	{
		$this->batch_move('categories');
	}

	public function censoring()
	{
		$this->batch_move('censoring');
	}

	public function config()
	{
		$ps = $this->old_db->select('config');
		foreach ($ps as $value)
		{
			switch ($value['conf_name'])
			{
				case 'o_board_title':
				case 'o_board_desc':
				case 'o_default_timezone':
				case 'o_time_format':
				case 'o_date_format':
				case 'o_timeout_visit':
				case 'o_timeout_online':
				case 'o_redirect_delay':
				case 'o_show_version':
				case 'o_show_user_info':
				case 'o_show_post_count':
				case 'o_signatures':
				case 'o_smilies':
				case 'o_smilies_sig':
				case 'o_make_links':
				case 'o_topic_review':
				case 'o_disp_topics_default':
				case 'o_disp_posts_default':
				case 'o_indent_num_spaces':
				case 'o_quote_depth':
				case 'o_quickpost':
				case 'o_ranks':
				case 'o_users_online':
				case 'o_censoring':
				case 'o_show_dot':
				case 'o_topic_views':
				case 'o_quickjump':
				case 'o_gzip':
				case 'o_additional_navlinks':
				case 'o_report_method':
				case 'o_regs_report':
				case 'o_default_email_setting':
				case 'o_mailing_list':
				case 'o_avatars':
				case 'o_avatars_width':
				case 'o_avatars_height':
				case 'o_avatars_size':
				case 'o_search_all_forums':
				case 'o_base_url':
				case 'o_admin_email':
				case 'o_webmaster_email':
				case 'o_smtp_host':
				case 'o_smtp_user':
				case 'o_smtp_pass':
				case 'o_smtp_ssl':
				case 'o_regs_allow':
				case 'o_regs_verify':
				case 'o_announcement':
				case 'o_announcement_message':
				case 'o_rules':
				case 'o_rules_message':
				case 'o_default_dst':
				case 'o_feed_type':
				case 'o_feed_ttl':
					// Do nothing, these are valid keys
				break;
				case 'o_subscriptions':
					$value['conf_name'] = 'o_topic_subscriptions';
				break;
				default:
					continue 2;
				break;
			}

			$update = array(
				'conf_value' => $value['conf_value'],
			);

			$data = array(
				':conf_name' => $value['conf_name'],
			);

			$this->db->update('config', $update, 'conf_name=:conf_name', $data);
		}
	}

	public function forums()
	{
		$this->batch_move('forums');
	}

	public function forum_perms()
	{
		$ps = $this->old_db->select('forum_perms');
		foreach ($ps as $cur_row)
		{
			$cur_row['group_id'] = $this->correct_group($cur_row['group_id']);
			$this->db->insert('forum_perms', $cur_row);
		}
	}

	public function forum_subscriptions()
	{
		$this->batch_move('forum_subscriptions');
	}

	public function topics($start, $limit)
	{
		$data = array(
			':start' => $start,
			':limit' => $limit,
		);

		$ps = $this->old_db->select('topics', '*', $data, 'id>:start ORDER BY id ASC LIMIT :limit');
		foreach ($ps as $cur_topic)
		{
			$start = $cur_topic['id'];
			$this->db->insert('topics', $cur_topic);
		}

		return $this->redirect('topics', 'id', $start);
	}

	public function posts($start, $limit)
	{
		$data = array(
			':start' => $start,
			':limit' => $limit,
		);

		$ps = $this->old_db->select('posts', '*', $data, 'id>:start ORDER BY id ASC LIMIT :limit');
		foreach ($ps as $cur_post)
		{
			$start = $cur_post['id'];
			$this->db->insert('posts', $cur_post);
		}

		return $this->redirect('posts', 'id', $start);
	}

	public function ranks()
	{
		$this->batch_move('ranks');
	}

	public function reports()
	{
		$this->batch_move('reports');
	}

	public function topic_subscriptions()
	{
		$ps = $this->old_db->select('subscriptions');
		foreach ($ps as $cur_subscription)
			$this->db->insert('topic_subscriptions', $cur_subscription);
	}
	
	public function groups()
	{
		$ps = $this->old_db->select('groups');
		foreach ($ps as $cur_group)
		{
			$cur_group['g_id'] = $this->correct_group($cur_group['g_id']);
			$data = array(
				':id' => $cur_group['g_id'],
			);

			$ps1 = $this->db->select('groups', 1, $data, 'g_id=:id');
			if ($ps1->rowCount())
				$this->db->update('groups', $cur_group, 'g_id=:id', $data);
			else
				$this->db->insert('groups', $cur_group);
		}
	}

	public function users($start, $limit)
	{
		$data = array(
			':start' => $start,
			':limit' => $limit,
		);

		$ps = $this->old_db->select('users', '*', $data, 'id!=1 AND id>:start ORDER BY id ASC LIMIT :limit');
		foreach ($ps as $cur_user)
		{
			$start = $cur_user['id'];
			$cur_user['group_id'] = $this->correct_group($cur_user['group_id']);
			unset($cur_user['linkedin']);
			unset($cur_user['skype']);
			unset($cur_user['jabber']);
			unset($cur_user['icq']);
			unset($cur_user['msn']);
			unset($cur_user['aim']);
			unset($cur_user['yahoo']);
			unset($cur_user['access_keys']);
			unset($cur_user['read_topics']);
			unset($cur_user['avatar']);
			unset($cur_user['avatar_width']);
			unset($cur_user['avatar_height']);

			$this->db->insert('users', $cur_user);
			generate_login_key($cur_user['id']);

			$filetypes = array('jpg', 'gif', 'png');
			foreach ($filetypes as $cur_type)
			{
				$path = $this->avatar_path.$cur_user['id'].'.'.$cur_type;
				if (file_exists($path) && $img_size = getimagesize($path))
					$this->save_avatar($path, $cur_user['id']);
			}
		}

		$this->redirect('users', 'id', $start);
	}
}