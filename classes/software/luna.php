<?php
if (!defined('PANTHER'))
	exit;

class luna extends panther
{
	public $steps = array(
		'bans',
		'categories',
		'censoring',
		'forums',
		'forum_perms',
		'forum_subscriptions',
		'posts',
		'reports',
		'topics',
		'ranks',
		'topic_subscriptions',
		'config',
		'groups',
		'users',
	);

	public $file = 'fluxbb-password-converter';
	private function batch_move($table)
	{
		$ps = $this->old_db->select($table);
		foreach ($ps as $cur_row)
			$this->db->insert($table, $cur_row);
	}

	private function correct_group($group_id)
	{
		switch ($group_id)
		{
			case 0:
			case 1:

			break;
			case 2:
				$group_id = 3;
			break;
			case 3:
				$group_id = 4;
			break;
			case 4:
				$group_id = 5;
			break;
			default: // Increment every other group because we have a 'new members' group as 6
				$group_id++;
			break;
		}

		return $group_id;
	}

	public function bans()
	{
		$this->batch_move('bans');
	}

	public function categories()
	{
		$this->batch_move('categories');
	}

	public function censoring()
	{
		$this->batch_move('censoring');
	}

	public function config()
	{
		$ps = $this->old_db->select('config');
		foreach ($ps as $key => $value)
		{
			switch ($value['conf_name'])
			{
				case 'o_board_title':
				case 'o_board_desc':
				case 'o_default_timezone':
				case 'o_time_format':
				case 'o_date_format':
				case 'o_timeout_visit':
				case 'o_timeout_online':
				case 'o_show_user_info':
				case 'o_show_post_count':
				case 'o_signatures':
				case 'o_smilies_sig':
				case 'o_make_links':
				case 'o_topic_review':
				case 'o_disp_topics_default':
				case 'o_disp_posts_default':
				case 'o_indent_num_spaces':
				case 'o_quote_depth':
				case 'o_users_online':
				case 'o_censoring':
				case 'o_topic_views':
				case 'o_gzip':
				case 'o_report_method':
				case 'o_regs_report':
				case 'o_default_email_setting':
				case 'o_mailing_list':
				case 'o_avatars':
				case 'o_avatars_width':
				case 'o_avatars_height':
				case 'o_avatars_size':
				case 'o_search_all_forums':
				case 'o_admin_email':
				case 'o_webmaster_email':
				case 'o_forum_subscriptions':
				case 'o_topic_subscriptions':
				case 'o_smtp_host':
				case 'o_smtp_user':
				case 'o_smtp_pass':
				case 'o_smtp_ssl':
				case 'o_regs_allow':
				case 'o_regs_verify':
				case 'o_announcement':
				case 'o_announcement_message':
				case 'o_rules':
				case 'o_rules_message':
				case 'o_default_dst':
				case 'o_feed_type':
				case 'o_feed_ttl':
				case 'p_message_img_tag':
				case 'p_message_all_caps':
				case 'p_subject_all_caps':
				case 'p_sig_all_caps':
				case 'p_sig_bbcode':
				case 'p_sig_img_tag':
				case 'p_sig_length':
				case 'p_sig_lines':
				case 'p_allow_banned_email':
				case 'p_allow_dupe_email':
				case 'p_force_guest_email':
					// Do nothing, these are valid keys
				break;
				case 'o_has_posted':
					$value['conf_name'] = 'o_show_dot';
				break;
				case 'o_pms_enabled':
					$value['conf_name'] = 'o_private_messaging';
				break;
				case 'o_admin_note':
					$value['conf_name'] = 'o_admin_notes';
				break;
				case 'o_pms_max_receiver':
					$value['conf_name'] = 'o_max_pm_receivers';
				break;
				default:
					continue 2;
				break;
			}

			$update = array(
				'conf_value' => $value['conf_value'],
			);
			
			$data = array(
				':conf_name' => $value['conf_name'],
			);
			
			$this->db->update('config', $update, 'conf_name=:conf_name', $data);
		}
	}

	public function forums()
	{
		$ps = $this->old_db->select('forums');
		foreach ($ps as $cur_forum)
		{
			if (isset($cur_forum['num_threads'])) // Then we are in fact using the 1.3 branch so things are slightly different ...
			{
				$cur_forum['num_topics'] = $cur_forum['num_threads'];
				$cur_forum['num_posts'] = $cur_forum['num_comments'];
				$cur_forum['last_post'] = $cur_forum['last_comment'];
				$cur_forum['last_post_id'] = $cur_forum['last_comment_id'];
				$cur_forum['last_poster_id'] = $cur_forum['last_commenter_id'];
			}

			$data = array(
				':id' => $cur_forum['last_poster_id'],
			);

			$ps1 = $this->old_db->select('users', 'username', $data, 'id=:id');
			$cur_forum['last_poster'] = $ps1->fetchColumn();

			$insert = array(
				'id' => $cur_forum['id'],
				'forum_name' => $cur_forum['forum_name'],
				'forum_desc' => $cur_forum['forum_desc'],
				'moderators' => $cur_forum['moderators'],
				'num_topics' => $cur_forum['num_topics'],
				'num_posts' => $cur_forum['num_posts'],
				'last_post' => $cur_forum['last_post'],
				'last_post_id' => $cur_forum['last_post_id'],
				'last_poster' => $cur_forum['last_poster'],
				'sort_by' => $cur_forum['sort_by'],
				'disp_position' => $cur_forum['disp_position'],
				'cat_id' => $cur_forum['cat_id'],
				'parent_forum' => $cur_forum['parent_id'],
			);
			
			$this->db->insert('forums', $insert);
		}
	}

	public function forum_perms()
	{
		$ps = $this->old_db->select('forum_perms');
		foreach ($ps as $cur_row)
		{
			$cur_row['group_id'] = $this->correct_group($cur_row['group_id']);
			if (isset($cur_row['comment'])) // 1.3 branch
			{
				$cur_row['post_replies'] = $cur_row['comment'];
				unset($cur_row['comment']);
				$cur_row['post_topics'] = $cur_row['create_threads'];
				unset($cur_row['create_threads']);
			}

			$this->db->insert('forum_perms', $cur_row);
		}
	}

	public function forum_subscriptions()
	{
		$this->batch_move('forum_subscriptions');
	}

	public function topics($start, $limit)
	{
		$data = array(
			':start' => $start,
			':limit' => $limit,
		);

		$table = ($this->old_db->table_exists('threads')) ? 'threads' : 'topics'; // 1.3 branch
		$ps = $this->old_db->select($table, '*', $data, 'id>:start ORDER BY id ASC LIMIT :limit');
		foreach ($ps as $cur_topic)
		{
			if ($table == 'threads')
			{
				$cur_topic['poster'] = $cur_topic['commenter'];
				$cur_topic['posted'] = $cur_topic['commented'];
				$cur_topic['first_post_id'] = $cur_topic['first_comment_id'];
				$cur_topic['last_post_id'] = $cur_topic['last_comment_id'];
				$cur_topic['last_poster'] = $cur_topic['last_commenter'];
				$cur_topic['last_poster_id'] = $cur_topic['last_commenter_id'];
				$cur_topic['sticky'] = $cur_topic['pinned'];
				unset($cur_topic['important']);
				unset($cur_topic['commenter']);
				unset($cur_topic['commented']);
				unset($cur_topic['first_comment_id']);
				unset($cur_topic['last_comment_id']);
				unset($cur_topic['last_commenter']);
				unset($cur_topic['last_comment']);
				unset($cur_topic['last_commenter_id']);
				unset($cur_topic['pinned']);
			}

			$start = $cur_topic['id'];
			$data = array(
				':id' => $cur_topic['last_poster_id'],
			);

			$ps1 = $this->old_db->select('users', 'username', $data, 'id=:id');
			$cur_topic['last_poster'] = $ps1->fetchColumn();
	
			$cur_topic['deleted'] = $cur_topic['soft'];
			unset($cur_topic['soft']);
			unset($cur_topic['last_poster_id']);
			unset($cur_topic['solved']);
			$this->db->insert('topics', $cur_topic);
		}

		return $this->redirect($table, 'id', $start);
	}

	public function posts($start, $limit)
	{
		$data = array(
			':start' => $start,
			':limit' => $limit,
		);

		$table = ($this->old_db->table_exists('comments')) ? 'comments' : 'posts'; // 1.3 branch
		$ps = $this->old_db->select($table, '*', $data, 'id>:start ORDER BY id ASC LIMIT :limit');
		foreach ($ps as $cur_post)
		{
			if ($table == 'comments')
			{
				$cur_post['poster'] = $cur_post['commenter'];
				$cur_post['poster_id'] = $cur_post['commenter_id'];
				$cur_post['poster_ip'] = $cur_post['commenter_ip'];
				$cur_post['poster_email'] = $cur_post['commenter_email'];
				$cur_post['posted'] = $cur_post['commented'];
				$cur_post['topic_id'] = $cur_post['thread_id'];
				unset($cur_post['commenter']);
				unset($cur_post['commenter_id']);
				unset($cur_post['commenter_ip']);
				unset($cur_post['commenter_email']);
				unset($cur_post['commented']);
				unset($cur_post['thread_id']);
			}

			$start = $cur_post['id'];
			$cur_post['deleted'] = $cur_post['soft'];
			unset($cur_post['soft']);
			unset($cur_post['marked']);
			$this->db->insert('posts', $cur_post);
		}

		return $this->redirect($table, 'id', $start);
	}

	public function reports()
	{
		$this->batch_move('reports');
	}

	public function topic_subscriptions()
	{
		if ($this->old_db->table_exists('thread_subscriptions'))
		{
			$ps = $this->old_db->select('thread_subscriptions');
			foreach ($ps as $cur_subscription)
			{
				$insert = array(
					'user_id' => $cur_subscription['user_id'],
					'topic_id' => $cur_subscription['thread_id'],
				);

				$this->db->insert('topic_subscriptions', $insert);
			}
		}
		else
			$this->batch_move('topic_subscriptions');
	}

	public function ranks()
	{
		$ps = $this->old_db->select('ranks');
		foreach ($ps as $cur_rank)
		{
			$insert = array(
				'id' => $cur_rank['id'],
				'rank' => $cur_rank['rank'],
				'min_posts' => isset($cur_rank['min_comments']) ? $cur_rank['min_comments'] : $cur_rank['min_posts'],
			);

			$this->db->insert('ranks', $insert);
		}
	}

	public function groups()
	{
		$ps = $this->old_db->select('groups');
		foreach ($ps as $cur_group)
		{
			if (isset($cur_group['g_comment_flood'])) // 1.3 branch
			{
				$cur_group['g_post_flood'] = $cur_group['g_comment_flood'];
				$cur_group['g_post_replies'] = $cur_group['g_comment'];
				$cur_group['g_post_topics'] = $cur_group['g_create_threads'];
				$cur_group['g_edit_posts'] = $cur_group['g_edit_comments'];
				$cur_group['g_delete_posts'] = $cur_group['g_delete_comments'];
				$cur_group['g_delete_topics'] = $cur_group['g_delete_threads'];
				$cur_group['g_use_pm'] = $cur_group['g_inbox'];

				unset($cur_group['g_inbox']);
				unset($cur_group['g_inbox_limit']);
				unset($cur_group['g_soft_delete_threads']);
				unset($cur_group['g_soft_delete_comments']);
				unset($cur_group['g_delete_threads']);
				unset($cur_group['g_delete_comments']);
				unset($cur_group['g_edit_comments']);
				unset($cur_group['g_create_threads']);
				unset($cur_group['g_comment']);
				unset($cur_group['g_comment_flood']);
			}
			else
			{
				$cur_group['g_use_pm'] = $cur_group['g_pm'];
				unset($cur_group['g_soft_delete_posts']);
				unset($cur_group['g_soft_delete_topics']);
				unset($cur_group['g_pm']);
			}
			unset($cur_group['g_soft_delete_view']);

			$cur_group['g_id'] = $this->correct_group($cur_group['g_id']);
			$data = array(
				':id' => $cur_group['g_id'],
			);

			$ps1 = $this->db->select('groups', 1, $data, 'g_id=:id');
			if ($ps1->rowCount())
				$this->db->update('groups', $cur_group, 'g_id=:id', $data);
			else
				$this->db->insert('groups', $cur_group);
		}
	}

	public function users($start, $limit)
	{
		$data = array(
			':start' => $start,
			':limit' => $limit,
		);

		$ps = $this->old_db->select('users', '*', $data, 'id!=1 AND id>:start ORDER BY id ASC LIMIT :limit');
		foreach ($ps as $cur_user)
		{
			$start = $cur_user['id'];
			if (isset($cur_user['use_inbox'])) // For the 1.3 branch
			{
				$cur_user['pm_enabled'] = $cur_user['use_inbox'];
				$cur_user['num_pms'] = $cur_user['num_inbox'];
				$cur_user['last_post'] = $cur_user['last_comment'];
				$cur_user['num_posts'] = $cur_user['num_comments'];
				$cur_user['timezone'] = $cur_user['php_timezone'];
				$cur_user['disp_topics'] = $cur_user['disp_threads'];
				$cur_user['disp_posts'] = $cur_user['disp_comments'];
				$cur_user['notify_with_post'] = $cur_user['notify_with_comment'];
				$cur_user['pm_notify'] = $cur_user['notify_inbox'];
				unset($cur_user['use_inbox']);
				unset($cur_user['notify_inbox']);
				unset($cur_user['notify_inbox_full']);
				unset($cur_user['disp_threads']);
				unset($cur_user['disp_comments']);
				unset($cur_user['notify_with_comment']);
				unset($cur_user['php_timezone']);
				unset($cur_user['num_comments']);
				unset($cur_user['last_comment']);
				unset($cur_user['num_inbox']);
			}
			else
			{
				$cur_user['pm_enabled'] = $cur_user['use_pm'];
				$cur_user['pm_notify'] = $cur_user['notify_pm'];
				unset($cur_user['notify_pm']);
				unset($cur_user['use_pm']);
				unset($cur_user['notify_pm_full']);
			}
			
			if (isset($cur_user['enforce_accent']))
				unset($cur_user['enforce_accent']);

			$cur_user['group_id'] = $this->correct_group($cur_user['group_id']);

			unset($cur_user['msn']);
			unset($cur_user['first_run']);
			unset($cur_user['color_scheme']);
			unset($cur_user['adapt_time']);
			unset($cur_user['accent']);

			$this->db->insert('users', $cur_user);
			generate_login_key($cur_user['id']);

			$filetypes = array('jpg', 'gif', 'png');
			foreach ($filetypes as $cur_type)
			{
				$path = $this->avatar_path.$cur_user['id'].'.'.$cur_type;
				if (file_exists($path) && $img_size = getimagesize($path))
					$this->save_avatar($path, $cur_user['id']);
			}
		}

		$this->redirect('users', 'id', $start);
	}
}