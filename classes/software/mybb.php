<?php
if (!defined('PANTHER'))
	exit;

class mybb extends panther
{
	public $steps = array(
		'announcements',
		'attachments',
		'bans',
		'censoring',
		'forums',
		'forum_perms',
		'forum_subscriptions',
		'multi_moderation',
		'posts',
		'robots',
		'reports',
		'topics',
		'topic_subscriptions',
		'warnings',
		'warning_types',
		'ranks',
		'config',
		'groups',
		'users',
	);

	private $matches = array(
		'%\[quote=\'(.*?)\'.*?\]\s*%si'								=>	'[quote=$1]',
		'%\[/?(font|size|align)(?:\=[^\]]*)?\]%i'					=>	'', // Just strip these; they're not supported
	);

	private $replacements = array(
		'[php]' => '[code]',
		'[/php]' => '[/code]',
	);

	public $file = 'mybb-password-converter';
	private function my_inet_ntop($ip)
	{
		if (function_exists('inet_ntop'))
			return @inet_ntop($ip);
		else
		{
			switch(strlen($ip))
			{
				case 4:
					list(,$r) = unpack('N', $ip);
					return long2ip($r);
				case 16:
					$r = substr(chunk_split(bin2hex($ip), 4, ':'), 0, -1);
					$r = preg_replace(
						array('/(?::?\b0+\b:?){2,}/', '/\b0+([^0])/e'),
						array('::', '(int)"$1"?"$1":"0$1"'),
						$r);
					return $r;
			}
			return false;
		}
	}

	private function correct_group($group_id)
	{
		switch($group_id)
		{
			case 1:
				$group_id = 4;
			break;
			case 2:
				$group_id = 5;
			break;
			case 3:
				$group_id = 2;
			break;
			case 4:
				$group_id = 1;
			break;	
			case 5:
			case 7:
				// Panther doesn't have an 'awaiting activation' or a 'banned' group
			break;
			break;	
			case 6:
				$group_id = 3;
			break;
		}

		return $group_id;
	}
	
	public function announcements()
	{
		$ps = $this->old_db->select('announcements', 'aid AS id, fid AS forum_id, uid+1 AS user_id, subject, message');
		foreach ($ps as $cur_announcement)
			$this->db->insert('announcements', $cur_announcement);
	}
	
	public function attachments()
	{
		$ps = $this->old_db->select('attachments', 'aid AS id, pid AS post_id, uid+1 AS owner, filename, filetype AS mime, filesize AS size, downloads, attachname AS location');
		foreach ($ps as $cur_attach)
		{
			$cur_attach['extension'] = substr($cur_attach['filename'], strrpos($cur_attach['filename'], '.')+1);

			$location = explode('/', $cur_attach['location']);
			$location = explode('_', $location[1]);
			$this->save_attachment($cur_attach['location'], $location[3]);
			$cur_attach['location'] = $location[3];

			$this->db->insert('attachments', $cur_attach);
		}
	}

	public function bans()
	{
		$ps = $this->old_db->run('SELECT u.username, u.lastip AS ip, u.email, b.reason AS message, b.lifted AS expire, admin+1 AS ban_creator FROM '.$this->old_db->prefix.'banned AS b LEFT JOIN '.$this->old_db->prefix.'users AS u ON b.uid=u.uid');
		foreach ($ps as $cur_ban)
		{
			$cur_ban['ip'] = $this->my_inet_ntop($cur_ban['ip']);
			$this->db->insert('bans', $cur_ban);
		}
	}

	public function censoring()
	{
		$ps = $this->old_db->select('badwords', 'bid AS id, badword AS search_for, replacement AS replace_with');
		foreach ($ps as $cur_word)
			$this->db->insert('censoring', $cur_word);
	}

	public function config()
	{
		$ps = $this->old_db->select('settings', 'name, value');
		foreach ($ps as $cur_config)
		{
			switch($cur_config['name'])
			{
				case 'boardclosed':
					$conf_name = 'o_maintenance';
				break;
				case 'boardclosed_reason':
					$conf_name = 'o_maintenance_message';
				break;
				case 'bbname':
					$conf_name = 'o_board_title';
				break;
				case 'adminemail':
					$conf_name = 'o_admin_email';
				break;
				case 'cookiedomain':
					$conf_name = 'o_cookie_domain';
				break;
				case 'cookieprefix':
					if ($cur_config['value'] == '')
						continue 2;

					$conf_name = 'o_cookie_name';
				break;
				case 'showvernum':
					$conf_name = 'o_show_version';
				break;
				case 'mailingaddress':
					$conf_name = 'o_mailing_list';
				break;
				case 'reportmethod':
					$conf_name = 'o_report_method';
					if ($cur_config['value'] == 'db')
						$cur_config['value'] = 0;
					else if ($cur_config['value'] == 'email')
						$cur_config['value'] = 1;
					else
						$cur_config['value'] = 2;
				break;
				case 'redirects':
					$conf_name = 'o_redirect_delay';
				break;
				case 'enableforumjump':
					$conf_name = 'o_quickjump';
				break;
				case 'gzipoutput':
					$conf_name = 'o_gzip';
				break;
				case 'dateformat':
					$conf_name = 'o_date_format';
				break;
				case 'timeformat':
					$conf_name = 'o_time_format';
				break;
				case 'timezoneoffset':
					$conf_name = 'o_default_timezone';
				break;
				case 'dstcorrection':
					$conf_name = 'o_default_dst';
				break;
				case 'threadsperpage':
					$conf_name = 'o_disp_topics_default';
				break;
				case 'hottopic':
					$conf_name = 'o_popular_topics';
				break;
				case 'postsperpage':
					$conf_name = 'o_disp_posts_default';
				break;
				case 'quickreply':
					$conf_name = 'o_quickpost';
				break;
				case 'disableregs':
					$conf_name = 'o_regs_allow';
				break;
				case 'sigimgcode':
					$conf_name = 'p_sig_img_tag';
				break;
				case 'siglength':
					$conf_name = 'p_sig_length';
				break;
				case 'maxquotedepth':
					$conf_name = 'o_quote_depth';
				break;
				case 'maxpolloptions':
					$conf_name = 'o_max_poll_fields';
				break;
				case 'soft_delete':
					$conf_name = 'o_delete_full';
				break;			
				case 'enableattachments':
					$conf_name = 'o_attachments';
				break;		
				case 'enablereputation':
					$conf_name = 'o_reputation';
				break;		
				case 'enablewarningsystem':
					$conf_name = 'o_warnings';
				break;		
				case 'allowcustomwarnings':
					$conf_name = 'o_custom_warnings';
				break;		
				case 'canviewownwarning':
					$conf_name = 'o_warning_status';
				break;		
				case 'enablepms':
					$conf_name = 'o_private_messaging';
				break;
				default:
					continue 2;
				break;
			}

			$update = array(
				'conf_value' => $cur_config['value'],
			);

			$data = array(
				':conf_name' => $conf_name,
			);

			$this->db->update('config', $update, 'conf_name=:conf_name', $data);
		}
	}

	public function forums()
	{
		$ps = $this->old_db->select('forums', 'fid AS id, name AS forum_name, description AS forum_desc, linkto AS redirect_url, type, pid AS parent_forum, disporder AS disp_position, threads AS num_topics, posts AS num_posts, lastpost AS last_post, lastposter AS last_poster, lastposttid AS last_topic_id, lastpostsubject AS last_topic, usepostcounts AS increment_posts, showinjump AS quickjump');
		foreach ($ps as $cur_forum)
		{
			if ($cur_forum['type'] == 'f') // It's a forum
			{
				unset($cur_forum['type']);
				$data = array(
					':id' => $cur_forum['parent_forum'],
				);

				$ps1 = $this->old_db->select('forums', 'type', $data, 'fid=:id');
				$type = $ps1->fetchColumn();
				if ($type == 'c')
				{
					$cur_forum['cat_id'] = $cur_forum['parent_forum'];
					$cur_forum['parent_forum'] = 0;
				}

				// Make sure it's properly set to NULL
				if ($cur_forum['redirect_url'] == '')
					$cur_forum['redirect_url'] = null;
				
				$moderators = array();
				$ps1 = $this->old_db->run('SELECT u.username, u.uid+1 AS id, u.usergroup AS group_id FROM '.$this->old_db->prefix.'moderators AS m INNER JOIN '.$this->old_db->prefix.'users AS u ON m.mid=u.uid');
				foreach ($ps1 as $cur_moderator)
				{
					$moderators[$cur_moderator['username']] = $cur_moderator['id'];
					if (!isset($moderators['groups']))
						$moderators['groups'] = array();

					$cur_moderator['group_id'] = $this->correct_group($cur_moderator['group_id']);
					$moderators['groups'][$cur_moderator['id']] = $cur_moderator['group_id'];
				}

				$cur_forum['moderators'] = (!empty($moderators)) ? serialize($moderators) : null;
				$this->db->insert('forums', $cur_forum);
			}
			else // It's a category
			{
				$cur_category = array(
					'id' => $cur_forum['id'],
					'cat_name' => $cur_forum['forum_name'],
					'disp_position' => $cur_forum['disp_position']
				);

				$this->db->insert('categories', $cur_category);
			}
		}
	}

	public function forum_perms()
	{
		$ps = $this->old_db->run('SELECT gid AS group_id, fid AS forum_id, canview AS read_forum, canpostreplys AS post_replies, canpostthreads AS post_topics, canpostpolls AS post_polls FROM '.$this->old_db->prefix.'forumpermissions');
		foreach ($ps as $cur_perm)
		{
			$data = array(
				':id' => $cur_perm['forum_id'],
			);

			$ps1 = $this->old_db->select('forums', 1, $data, 'fid=:id AND type=\'f\'');
			if (!$ps1->rowCount()) // Then it's a category
				continue;

			$cur_perm['group_id'] = $this->correct_group($cur_perm['group_id']);
			$this->db->insert('forum_perms', $cur_perm);
		}
	}

	public function forum_subscriptions()
	{
		// We don't want any 'category' subscriptions, so we need to do this query
		$ps = $this->old_db->run('SELECT f.fid AS forum_id, f.uid AS user_id FROM '.$this->old_db->prefix.'forumsubscriptions AS f INNER JOIN '.$this->old_db->prefix.'forums AS f2 ON f2.type=\'f\'');
		foreach ($ps as $cur_subscription)
			$this->db->insert('forum_subscriptions', $cur_subscription);
	}
	
	public function multi_moderation()
	{
		$ps = $this->old_db->select('modtools', 'tid AS id, name AS title, threadoptions');
		foreach ($ps as $cur_moderation)
		{
			$options = unserialize($cur_moderation['threadoptions']);
			switch($options['openthread'])
			{
				case 'open':
					$options['open'] = 1;
				break;
				case 'close':
					$options['open'] = 0;
				break;
				default:
					$options['open'] = 2;
				break;
			}

			switch($options['stickthread'])
			{
				case 'stick':
					$options['stick'] = 1;
				break;
				case 'unstick':
					$options['stick'] = 0;
				break;
				default:
					$options['stick'] = 2;
				break;
			}
			
			if ($options['addreply'] != '')
			{
				$options['add_reply'] = 1;
				$options['reply_message'] = $options['addreply'];
			}
			else
			{
				$options['add_reply'] = '';
				$options['reply_message'] = '';
			}
			
			$cur_moderation = array(
				'id' => $cur_moderation['id'],
				'title' => $cur_moderation['title'],
				'close' => $options['open'],
				'stick' => $options['stick'],
				'move' => $options['movethread'],
				'archive' => 2,
				'leave_redirect' => $options['movethreadredirect'],
				'add_reply' => $options['add_reply'],
				'reply_message' => $options['reply_message'],
			);

			$this->db->insert('multi_moderation', $cur_moderation);
		}
	}

	public function topics($start, $limit)
	{
		$data = array(
			':start' => $start,
			':limit' => $limit,
		);

		$ps = $this->old_db->select('threads', 'poll, tid AS id, fid AS forum_id, subject, uid+1 AS poster, username AS poster, dateline AS posted, firstpost AS first_post_id, lastpost AS last_post, lastposter AS last_poster, views AS num_views, replies AS num_replies, closed, sticky, visible AS approved', $data, 'tid>:start ORDER BY tid ASC LIMIT :limit');
		foreach ($ps as $cur_topic)
		{
			$start = $cur_topic['id'];
			if ($cur_topic['poll'])
			{
				$data = array(
					':id' => $cur_topic['poll'],
				);

				$ps1 = $this->old_db->select('polls', 'pid AS id, tid AS topic_id, question, multiple AS type, options, votes', $data, 'pid=:id');
				$cur_poll = $ps1->fetch();

				$cur_topic['question'] = $cur_poll['question'];
				unset($cur_poll['question']);
				$cur_poll['type'] = ($cur_poll['type'] == 1) ? 2 : 1;
				$cur_poll['options'] = serialize(explode('||~|~||', $cur_poll['options']));
				$cur_poll['votes'] = serialize(explode('||~|~||', $cur_poll['votes']));
				
				$voters = array();
				$data = array(
					':id' => $cur_poll['id'],
				);

				$ps1 = $this->old_db->select('pollvotes', 'uid+1 AS uid', $data, 'pid=:id');
				$ps1->setFetchMode(PDO::FETCH_COLUMN, 0);
				foreach ($ps1 as $cur_voter)
					$voters[] = $cur_voter;

				$cur_poll['voters'] = serialize($voters);
				$this->db->insert('polls', $cur_poll); 
			}

			unset($cur_topic['poll']);
			$this->db->insert('topics', $cur_topic);
		}

		return $this->redirect('threads', 'tid', $start);
	}

	public function posts($start, $limit)
	{
		$data = array(
			':start' => $start,
			':limit' => $limit,
		);

		$ps = $this->old_db->select('posts', 'pid AS id, username AS poster, uid+1 AS poster_id, message, dateline AS posted, ipaddress AS poster_ip, smilieoff AS hide_smilies, edittime AS edited, edituid+1 AS edited_by, editreason AS edit_reason, visible AS approved, tid AS topic_id', $data, 'pid>:start ORDER BY pid ASC LIMIT :limit');
		foreach ($ps as $cur_post)
		{
			$start = $cur_post['id'];
			$data = array(
				':id' => $cur_post['edited_by'],
			);

			$cur_post['message'] = preg_replace(array_keys($this->matches), array_values($this->matches), $cur_post['message']);
			$cur_post['message'] = str_replace(array_keys($this->replacements), array_values($this->replacements), $cur_post['message']);
			$ps1 = $this->old_db->select('users', 'username', $data, 'uid=:id');
			$cur_post['edited_by'] = $ps1->fetchColumn();

			$cur_post['poster_ip'] = $this->my_inet_ntop($cur_post['poster_ip']);
			$this->db->insert('posts', $cur_post);
		}

		return $this->redirect('posts', 'pid', $start);
	}
	
	public function robots()
	{
		$ps = $this->old_db->select('questions', 'question, answer');
		foreach ($ps as $cur_test)
		{
			$cur_test['answer'] = str_replace(array("\n", "\r"), '', $cur_test['answer']);
			$this->db->insert('robots', $cur_test);
		}
	}
	
	public function topic_subscriptions()
	{
		$ps = $this->old_db->select('threadsubscriptions', 'tid AS topic_id, uid+1 AS user_id');
		foreach ($ps as $cur_subscription)
			$this->db->insert('topic_subscriptions', $cur_subscription);
	}

	public function ranks()
	{
		$ps = $this->old_db->select('usertitles', 'utid AS id, posts AS min_posts, title AS rank');
		foreach ($ps as $cur_rank)
			$this->db->insert('ranks', $cur_rank);
	}

	public function reports()
	{
		$ps = $this->old_db->select('reportedcontent', 'id AS topic_id, id2 AS topic_id, id3 AS forum_id, uid+1 AS reported_by, dateline AS created, reason AS message');
		foreach ($ps as $cur_report)
			$this->db->insert('reports', $cur_report);
	}

	public function groups()
	{
		$ps = $this->old_db->select('usergroups', 'gid AS g_id, title AS g_title, usertitle AS g_user_title, canviewprofiles AS g_view_users, canview AS g_read_board, canpostthreads AS g_post_topics, canpostreplys AS g_post_replies, caneditposts AS g_edit_posts, candeleteposts AS g_delete_posts, candeletethreads AS g_delete_topics, canpostpolls AS g_post_polls, canusepms AS g_use_pm, pmquota AS g_pm_limit, cansendemail AS g_send_email, emailfloodtime AS g_email_flood, canviewmemberlist AS g_search_users, cancp AS g_admin, issupermod AS g_global_moderator, cansearch AS g_search, usereputationsystem AS g_rep_enabled, maxreputationsperuser AS g_rep_plus, attachquota AS g_max_size, cancustomtitle AS g_set_title, canwarnusers AS g_mod_warn_users, canmodcp AS g_mod_cp, edittimelimit AS g_deledit_interval, caneditprofiles AS g_mod_edit_users, canbanusers AS g_mod_ban_users');
		foreach ($ps as $cur_group)
		{
			$group_id = $this->correct_group($cur_group['g_id']);
			$data = array(
				':id' => $group_id,
			);

			$this->db->select('groups', 1, $data, 'g_id=:id');
			if ($ps->rowCount())
			{
				unset($cur_group['g_id']);
				$this->db->update('groups', $cur_group, 'g_id=:id', $data);
			}
			else
			{
				$cur_group['g_id'] = $group_id;
				$this->db->insert('groups', $cur_group);
			}
		}
	}
	
	public function warnings()
	{
		$ps = $this->old_db->select('warnings', 'wid AS id, uid+1 AS user_id, tid AS type_id, pid AS post_id, title, points, dateline AS date_issued, issuedby+1 AS issued_by, expires AS date_expire, expired, notes AS note_admin');
		foreach ($ps as $cur_warning)
			$this->db->insert('warnings', $cur_warning);
	}
	
	public function warning_types()
	{
		$ps = $this->old_db->select('warningtypes', 'tid AS id, title, points, expirationtime AS expiration_time');
		foreach ($ps as $cur_type)
			$this->db->insert('warning_types', $cur_type);
	}

	public function users($start, $limit)
	{
		$data = array(
			':start' => $start,
			':limit' => $limit,
		);

		$ps = $this->old_db->select('users', 'uid+1 AS id, username, password, salt, email, postnum AS num_posts, usergroup AS group_id, usertitle AS title, regdate AS registered, lastactive AS last_visit, lastpost AS last_post, website AS url, google, signature, receivepms AS pm_enabled, pmnotify AS pm_notify, showimages AS show_img, showsigs AS show_sig, showavatars AS show_avatars, dateformat AS date_format, timeformat AS time_format, dst, regip AS registration_ip', $data, 'uid>:start ORDER BY uid ASC LIMIT :limit');
		foreach ($ps as $cur_user)
		{
			$start = $cur_user['id'];
			$cur_user['group_id'] = $this->correct_group($cur_user['group_id']);
			$cur_user['registration_ip'] = $this->my_inet_ntop($cur_user['registration_ip']);

			$this->db->insert('users', $cur_user);
			generate_login_key($cur_user['id']);

			$filetypes = array('jpg', 'gif', 'png');
			foreach ($filetypes as $cur_type)
			{
				$path = $this->avatar_path.'avatar_'.$cur_user['id'].'.'.$cur_type;
				if (file_exists($path) && $img_size = getimagesize($path))
					$this->save_avatar($path, $cur_user['id']);
			}
		}

		$this->redirect('users', 'uid', $start);
	}
}